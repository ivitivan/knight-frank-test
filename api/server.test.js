import test from 'tape'
import request from 'supertest'
import server from './server'

test('Server', (t) => {
  t.plan(1)

  request(server)
    .post('/requests')
    .send({
      appartmentId: 1,
      name: 'Mike',
      phone:  '+7 (111) 123-45-67',
      email: 'mike@example.com',
    })
    .end((err, res) => {
      if (err) {
        t.end(err)
      } else {
        t.equal(res.status, 200)
      }
    })
})
