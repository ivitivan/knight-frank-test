import dotenv from 'dotenv'

dotenv.config()

import express from 'express'
import bodyParser from 'body-parser'
import {Pool} from 'pg'
import {sendSqlResponse, sendSqlError} from './utils/makeResponse'

const app = express()

app.use(bodyParser.json())
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', process.env.ALLOW_ORIGIN)
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
})

app.get('/buildings', (req, res) => {
  const getBuildingSql = `
    SELECT h.id, h.address, count(*) AS count_apartments
    FROM houses h
    INNER JOIN apartments a ON h.id = a.house_id
    GROUP BY h.id
  `

  pool.query(getBuildingSql)
    .then(sendSqlResponse(res))
    .catch(sendSqlError(res))
})

app.get('/buildings/:id', (req, res) => {
  const getApartmentsByHouse = `
    SELECT *
    FROM apartments
    WHERE house_id = $1
  `

  pool.query(getApartmentsByHouse, [req.params.id])
    .then(sendSqlResponse(res))
    .catch(sendSqlError(res))
})

app.get('/count_buildings', (req, res) => {
  const getCountBuildings = `
    SELECT count(*)
    FROM houses
  `

  pool.query(getCountBuildings)
    .then(sendSqlResponse(res, (sqlRes) => sqlRes.rows[0].count))
    .catch(sendSqlError(res))
})

app.get('/count_apartments', (req, res) => {
  const getCountApartments = `
    SELECT count(*)
    FROM apartments
  `

  pool.query(getCountApartments)
    .then(sendSqlResponse(res, (sqlRes) => sqlRes.rows[0].count))
    .catch(sendSqlError(res))
})

app.post('/requests', (req, res) => {
  const {
    apartmentId,
    name,
    phone,
    email,
  } = req.body

  res.status(200).send({
    data: {
      apartmentId,
      name,
      phone,
      email,
    },
  })
})

export default app
