export const sendSqlResponse = (res, sqlResParser = (i) => i.rows) => (sqlRes) => {
  const data = sqlResParser(sqlRes)

  res.send({
    data,
  })
}

export const sendSqlError = (res) => (error) => {
  res.send({
    error,
  })
}
