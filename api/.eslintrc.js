module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
  },
  extends: 'eslint:recommended',
  rules: {
    semi: ['error', 'never'],
    'eol-last': ['error', 'always'],
    'linebreak-style': ['error', 'unix'],
    'no-multiple-empty-lines': ['error', {max: 1, maxEOF: 1}],
    'no-unexpected-multiline': ['error'],
    'no-trailing-spaces': ['error'],
    'comma-dangle': ['error', 'always-multiline'],
    'arrow-parens': ['error', 'always'],
  },
}
