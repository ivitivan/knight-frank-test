import { Component, OnInit } from '@angular/core';
import { Building } from '../../types/building/building'
import {BuildingsService} from '../../services/buildings/buildings.service'

@Component({
  selector: 'buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.css']
})
export class BuildingsComponent implements OnInit {
  constructor(private buildingsService: BuildingsService) {}

  buildings;

  getBuildings(): void {
    this.buildingsService.getBuildings()
      .then(buildings => this.buildings = buildings);
  }

  ngOnInit(): void {
    this.getBuildings();
  }
}
