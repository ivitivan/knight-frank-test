import { Component, OnInit } from '@angular/core';
import {BuildingsService} from '../../services/buildings/buildings.service'
import {ApartmentsService} from '../../services/apartments/apartments.service'

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(
    private buildingsService: BuildingsService,
    private apartmentsService: ApartmentsService,
  ) {}

  countBuildings: number;
  countApartments: number;

  getCountBuildings(): void {
    this.buildingsService.getCountBuildings()
      .then(countBuildings => this.countBuildings = countBuildings);
  }

  getCountApartments(): void {
    this.apartmentsService.getCountApartments()
      .then(countApartments => this.countApartments = countApartments);
  }

  ngOnInit(): void {
    this.getCountBuildings();
    this.getCountApartments();
  }
}
