import { Component, OnInit } from '@angular/core';
import {Apartment} from '../../types/appartment/apartment'
import {ApartmentsService} from '../../services/apartments/apartments.service'
import {RequestService} from '../../services/request/request.service'
import {Router, ParamMap, ActivatedRoute} from '@angular/router'
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.css']
})
export class BuildingComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private apartmentsService: ApartmentsService,
    private requestService: RequestService,
  ) {}

  apartments;
  selectedApartmentId : number;
  showForm : boolean = false;

  getApartments(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.apartmentsService.getApartmentsByBuilding(+params.get('id')))
      .subscribe(apartments => this.apartments = apartments);
  }

  ngOnInit(): void {
    this.getApartments();
  }

  handleButtonClick(apartmentId: number): void {
    this.showForm = true;
    this.selectedApartmentId = apartmentId;
  }

  handleFormClose = (): void => {
    this.showForm = false;
    this.selectedApartmentId = undefined;
  }

  handleSubmit = (values): void => {
    this.requestService.createRequest(values)
  }
}
