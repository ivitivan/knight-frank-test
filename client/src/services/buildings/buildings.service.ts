import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import 'rxjs/add/operator/toPromise'
import { Building } from '../../types/building/building'
import {environment} from '../../environments/environment'

@Injectable()
export class BuildingsService {
  constructor(private http: Http) {}

  getBuildings(): Promise<Building[]> {
    return this.http.get(`${environment.apiBase}/buildings`)
      .toPromise()
      .then(response => response.json().data as Building[])
  }

  getCountBuildings(): Promise<number> {
    return this.http.get(`${environment.apiBase}/count_buildings`)
      .toPromise()
      .then(response => response.json().data)
  }
}
