import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import 'rxjs/add/operator/toPromise'
import {Apartment} from '../../types/appartment/apartment'
import {environment} from '../../environments/environment'

@Injectable()
export class ApartmentsService {
  constructor(private http: Http) {}

  getApartmentsByBuilding(buildingId: number): Promise<Apartment[]> {
    return this.http.get(`${environment.apiBase}/buildings/${buildingId}`)
      .toPromise()
      .then(response => response.json().data as Apartment[])
  }

  getCountApartments(): Promise<number> {
    return this.http.get(`${environment.apiBase}/count_apartments`)
      .toPromise()
      .then(response => response.json().data)
  }
}
