import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import {Request} from '../../types/request/request'
import 'rxjs/add/operator/toPromise'
import {environment} from '../../environments/environment'

@Injectable()
export class RequestService {
  constructor(private http: Http) {}

  createRequest(data: Request): Promise<Request[]> {
    return this.http.post(`${environment.apiBase}/requests`, data)
      .toPromise()
      .then(response => response.json().data as Request[])
  }
}
