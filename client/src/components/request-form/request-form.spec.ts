import { TestBed, async } from '@angular/core/testing';
import {RequestFormComponent} from './request-form.component'
import {FormsModule} from '@angular/forms'

describe('RequestFormComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
      ],
      declarations: [
        RequestFormComponent
      ],
    }).compileComponents();
  }));

  it('should call onSubmit when submitting', async(() => {
    const fixture = TestBed.createComponent(RequestFormComponent);
    const requestForm = fixture.debugElement.componentInstance;

    requestForm.onSubmit = () => {}
    requestForm.apartmentId = 1

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      spyOn(requestForm, 'onSubmit');

      let button = fixture.debugElement.nativeElement.querySelector('.request-form-submit');

      button.click();
      expect(requestForm.onSubmit).toHaveBeenCalled();
    })
  }));
});
