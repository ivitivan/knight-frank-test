import { Component, Input, OnInit } from '@angular/core';
import {Request} from '../../types/request/request'

@Component({
  selector: 'request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent implements OnInit {
  @Input() apartmentId: number;
  @Input() onClose: Function;
  @Input() onSubmit: Function;

  fields: Request;
  step: number = 0;

  ngOnInit(): void {
    this.fields = new Request(this.apartmentId);
  }

  handleSubmit() {
    this.onSubmit && this.onSubmit(this.fields);
    this.step = 1;
  }
}
