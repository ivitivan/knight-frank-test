export class Request {
  constructor(apartmentId: number) {
    this.apartmentId = apartmentId;
  }

  apartmentId: number;
  name: string;
  phone: string;
  email: string;
}
