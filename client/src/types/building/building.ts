export class Building {
  id: number;
  address: string;
  countApartments: number;
}
