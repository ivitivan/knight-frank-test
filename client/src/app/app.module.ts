import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http'
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import {HomeComponent} from '../pages/home/home.component'
import {BuildingsComponent} from '../pages/buildings/buildings.component'
import {BuildingComponent} from '../pages/building/building.component'
import {BuildingsService} from '../services/buildings/buildings.service'
import {ApartmentsService} from '../services/apartments/apartments.service'
import {RequestService} from '../services/request/request.service'
import {RequestFormComponent} from '../components/request-form/request-form.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'buildings',
    component: BuildingsComponent,
  },
  {
    path: 'buildings/:id',
    component: BuildingComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BuildingsComponent,
    BuildingComponent,
    RequestFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    FormsModule,
  ],
  providers: [BuildingsService, ApartmentsService, RequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
