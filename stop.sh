docker stop $(docker ps | grep ivanov-vitaly-knight-frank-test-database | awk '{print $1}')
docker stop $(docker ps | grep ivanov-vitaly-knight-frank-test-api | awk '{print $1}')
docker stop $(docker ps | grep ivanov-vitaly-knight-frank-test-client | awk '{print $1}')
