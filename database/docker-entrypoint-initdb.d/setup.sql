CREATE TABLE houses (
  id SERIAL PRIMARY KEY,
  address VARCHAR(1000)
);

INSERT INTO houses (
  id,
  address
) SELECT 1, 'Москва, Никитская Б. улица, д. 45'
  UNION
  SELECT 2, 'Санкт-Петербург, ул. Ленина, д. 7';

CREATE TABLE apartments (
  id SERIAL PRIMARY KEY,
  house_id INTEGER REFERENCES houses (id),
  apartment_size INTEGER
);

INSERT INTO apartments (
  id,
  house_id,
  apartment_size
) SELECT 1, 1, 95
  UNION
  SELECT 2, 1, 72
  UNION
  SELECT 3, 1, 102
  UNION
  SELECT 4, 2, 63
  UNION
  SELECT 5, 2, 78;
